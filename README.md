# Forex News Crawler based on Newsplease

### Getting started

Create virtual environment (optional)
```shell script
python3 -m venv venv
source venv/bin/activate
```

Install packages
```shell script
pip install git+https://bitbucket.org/luijkr/news-please-forex-crawler.git
pip install -U -e /Users/r.luijk/Documents/bitbucket/personal/news-please-forex-crawler
pip install chardet==3.0.2
pip install cchardet
```

Start up Docker container with Postgres 
```shell script
docker-compose up -d
```

Create tables by using code in ```init-postgresql-db.sql```

Run crawler
```shell script
news-please -c config/
```


### Configuration in config/ folder

- ```config_lib.cfg```: DO NOT TOUCH
- ```config.cfg```: settings used for the scraper. For more info, see original [documentation](https://github.com/fhamborg/news-please/wiki/configuration).
- ```sitelist.hjson```: list of all URLs to be scraped. These are URLs of the RSS feeds, which will be scraped by the edited ```RssCrawler``` class.